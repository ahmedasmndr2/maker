module gitlab.com/crankykernel/maker

require (
	github.com/gobuffalo/buffalo-plugins v1.8.2 // indirect
	github.com/gobuffalo/packr v1.21.5
	github.com/gobuffalo/packr/v2 v2.0.0-rc.9 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/gorilla/websocket v1.2.0
	github.com/inconshreveable/mousetrap v1.0.0
	github.com/mattn/go-sqlite3 v1.9.0
	github.com/mitchellh/go-homedir v1.0.0 // indirect
	github.com/oklog/ulid v0.3.0
	github.com/onsi/ginkgo v1.7.0 // indirect
	github.com/onsi/gomega v1.4.3 // indirect
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	github.com/spf13/viper v1.2.1
	gitlab.com/crankykernel/cryptotrader v0.0.0-20180706233839-e82c919dcb46
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
	golang.org/x/net v0.0.0-20181201002055-351d144fa1fc // indirect
	golang.org/x/sys v0.0.0-20181128092732-4ed8d59d0b35 // indirect
	gopkg.in/airbrake/gobrake.v2 v2.0.9 // indirect
	gopkg.in/gemnasium/logrus-airbrake-hook.v2 v2.1.2 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
